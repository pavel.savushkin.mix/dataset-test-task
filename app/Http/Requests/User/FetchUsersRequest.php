<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class FetchUsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'category_id' => 'exists:categories,id',
            'gender_id' => 'exists:genders,id',
            'birth_date' => 'date_format:Y-m-d',
            'age_range' => 'array|min:1|max:2',
            'age_range.*' => 'integer|min:0',
        ];
    }
}
