<?php

namespace App\Http\Controllers;

use App\DTO\Requests\AgeRange;
use App\Http\Requests\User\FetchUsersRequest;
use App\Http\Requests\User\StoreUserRequest;
use App\Http\Resources\UserResource;
use App\Jobs\ProcessDatasetJob;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param FetchUsersRequest $request
     *
     * @return AnonymousResourceCollection
     */
    public function index(FetchUsersRequest $request)
    {
        $items = $this->prepareItemsQuery($request)->paginate();

        return UserResource::collection($items);
    }

    /**
     * Prepares the query to fetch items.
     *
     * @param FetchUsersRequest $request
     *
     * @return EloquentBuilder
     */
    protected function prepareItemsQuery(FetchUsersRequest $request): EloquentBuilder
    {
        $itemsQuery = User::query();

        $request->whenFilled('category_id', function ($value) use ($itemsQuery) {
            $itemsQuery->where('category_id', $value);
        });
        $request->whenFilled('gender_id', function ($value) use ($itemsQuery) {
            $itemsQuery->where('gender_id', $value);
        });
        $request->whenFilled('birth_date', function ($value) use ($itemsQuery) {
            $itemsQuery->whereDate('birth_date', $value);
        });
        $request->whenFilled('age_range', function ($value) use ($itemsQuery) {
            $ageRange = new AgeRange();
            $ageRange->setFrom(min($value));
            $ageRange->setTo(max($value));

            $itemsQuery->byAgeRange($ageRange);
        });

        return $itemsQuery;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUserRequest $request)
    {
        $file = $request->file('file');

        $datasetName = Str::uuid() . '.' . $file->extension();
        $file->storeAs('', $datasetName);

        $filePath = Storage::path($datasetName);

        dispatch((new ProcessDatasetJob($filePath)));

        return response()->noContent();
    }

    /**
     * Exports the dataset.
     *
     * @param FetchUsersRequest $request
     *
     * @return JsonResponse
     */
    public function export(FetchUsersRequest $request)
    {
        $chunkSize = config('processing.chunk_size');
        $file = tmpfile();

        fwrite($file, 'category,firstname,lastname,email,gender,birthDate' . PHP_EOL);

        $this->prepareItemsQuery($request)
            ->with(['category', 'gender'])
            ->lazy($chunkSize)
            ->each(function (User $item) use ($file) {
                $line = [
                    $item->category->name,
                    $item->first_name,
                    $item->last_name,
                    $item->email,
                    $item->gender->name,
                    $item->birth_date->format('Y-m-d'),
                    PHP_EOL,
                ];

                fwrite($file, implode(',', $line));
            });

        $fileName = Str::uuid() . '.csv';
        $storage = Storage::drive('public');
        $storage->put($fileName, $file);

        fclose($file);

        return response()->json([
            'url' => $storage->url($fileName),
        ]);
    }
}
