<?php

namespace App\Http\Controllers;

class IndexController extends Controller
{
    /**
     * Shows main page of the application.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function index()
    {
        return view('index');
    }
}
