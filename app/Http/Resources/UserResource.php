<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $data = [
            'id' => $this->id,
            'category' => [
                'id' => $this->category_id,
            ],
            'gender' => [
                'id' => $this->gender_id,
            ],
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'birth_date' => $this->birth_date->format('Y-m-d'),
        ];

        if ($this->relationLoaded('category')) {
            $data['category'] = new CategoryResource($this->category);
        }

        if ($this->relationLoaded('gender')) {
            $data['gender'] = new GenderResource($this->gender);
        }

        return $data;
    }
}
