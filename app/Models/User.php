<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\DTO\Requests\AgeRange;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'category_id',
        'gender_id',
        'first_name',
        'last_name',
        'email',
        'birth_date',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'birth_date' => 'datetime:Y-m-d',
    ];

    /**
     * Returns category of the current user.
     *
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Returns gender of the current user.
     *
     * @return BelongsTo
     */
    public function gender(): BelongsTo
    {
        return $this->belongsTo(Gender::class);
    }

    /**
     * Filters the query by the provided age range.
     *
     * @param EloquentBuilder|QueryBuilder $builder
     * @param AgeRange $ageRange
     *
     * @return EloquentBuilder|QueryBuilder
     */
    public function scopeByAgeRange(EloquentBuilder|QueryBuilder $builder, AgeRange $ageRange): EloquentBuilder|QueryBuilder
    {
        $fromYear = now()->subYears($ageRange->getTo());
        $toYear = now()->subYears($ageRange->getFrom());

        if ($fromYear->isSameYear($toYear)) {
            $builder->whereYear('birth_date', $fromYear->format('Y'));
        } else {
            $builder->whereYear('birth_date', '>=', $fromYear->format('Y'))
                ->whereYear('birth_date', '<=', $toYear->format('Y'));
        }

        return $builder;
    }
}
