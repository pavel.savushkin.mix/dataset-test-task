<?php

namespace App\Jobs;

use App\Models\Category;
use App\Models\Gender;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\LazyCollection;

class ProcessDatasetJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Collection
     */
    protected $categories;

    /**
     * @var Collection
     */
    protected $genders;

    /**
     * Create a new job instance.
     *
     * @param string $datasetFilename
     */
    public function __construct(protected string $datasetFilename)
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $this->fetchCategories();
        $this->fetchGenders();

        $chunkSize = config('processing.chunk_size');

        LazyCollection::make(function () {
            $handle = fopen($this->datasetFilename, 'r');

            while (($line = fgets($handle)) !== false) {
                yield $line;
            }
        })
            ->skip(1)
            ->chunk($chunkSize)
            ->each(function (LazyCollection $lines) {
                // Convert each line to array with data
                $items = $lines->map(function (string $line) {
                    $items = explode(',', $line);

                    // Filter those items, where some details are missed
                    if (count($items) !== 6) {
                        return null;
                    }

                    // Remove new line from the end of the data
                    $items[5] = rtrim($items[5]);

                    return $items;
                })
                    ->filter()
                    ->values();

                // Replace category names with their IDs
                $items = $this->processCategories($items);
                // Replace gender names with their IDs
                $items = $this->processGenders($items);

                $itemsToProcess = $items->map(function (array $item) {
                    return [
                        'category_id' => $item[0],
                        'first_name' => $item[1],
                        'last_name' => $item[2],
                        'email' => $item[3],
                        'gender_id' => $item[4],
                        'birth_date' => $item[5],
                    ];
                })
                    ->toArray();

                dispatch(new ProcessDatasetBatchJob($itemsToProcess));
            });

        // Delete the processed file.
        Storage::delete(File::basename($this->datasetFilename));
    }

    /**
     * Fetches all available categories.
     *
     * @return void
     */
    protected function fetchCategories(): void
    {
        $this->categories = Category::query()->pluck('id', 'name');
    }

    /**
     * Fetches all available categories.
     *
     * @return void
     */
    protected function fetchGenders(): void
    {
        $this->genders = Gender::query()->pluck('id', 'name');
    }

    /**
     * Creates new categories if they are not exist and replaces the category names with the IDs.
     *
     * @param LazyCollection $items
     *
     * @return Collection
     */
    protected function processCategories(LazyCollection $items): LazyCollection
    {
        // Grub the category names
        $categories = $items
            ->map(function (array $item) {
                return $item[0];
            })
            ->unique()
            ->values();

        // Get new categories, that are not present in DB
        /** @var array $newCategoryNames */
        $newCategoryNames = $categories->diff($this->categories->keys())
            ->map(function ($category) {
                return ['name' => $category];
            })
            ->toArray();

        // Create new categories and fill them to the pre-fetched list
        Category::query()->insert($newCategoryNames);
        $this->categories = $this->categories->merge(Category::query()->whereIn('name', $newCategoryNames)->pluck('id', 'name'));

        // Update the category names with their IDs
        return $items->map(function (array $item) {
            $item[0] = $this->categories->get($item[0]);

            return $item;
        });
    }

    /**
     * Replaces the genders with their IDs or set it to `none` if the specified value is not found in DB.
     *
     * @param LazyCollection $items
     *
     * @return LazyCollection
     */
    protected function processGenders(LazyCollection $items): LazyCollection
    {
        return $items->map(function (array $item) {
            $item[4] = $this->genders->get($item[4], $this->genders['none']);

            return $item;
        });
    }
}
