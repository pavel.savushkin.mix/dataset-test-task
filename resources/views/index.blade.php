<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Dataset Test Task</title>
    @vite(['resources/css/app.scss', 'resources/js/app.js'])
</head>
<body>
<div class="app">
    <form ref="uploadForm"
          action="{{ route('users.store') }}"
          data-fetch-items="{{ route('users.index') }}"
          data-export-items="{{ route('users.export') }}"
          data-fetch-categories="{{ route('categories.index') }}"
          data-fetch-genders="{{ route('genders.index') }}"
          @submit.prevent="upload"
    >
        <input v-model="file"
               type="file"
               accept=".csv,.txt"
               :disabled="isLoading"
               @change="fileChanged"
        >
        <button type="submit"
                class="button button_success"
                :disabled="isLoading"
        >
            Upload
        </button>
    </form>
    <form class="app__filters filters" @submit.prevent="fetchItems">
        <div class="filters__item">
            <label for="category" class="filters__item-title">Category</label>
            <select v-model="filters.category_id" class="select" id="category" :disabled="isLoading">
                <option v-for="category in filterCategories" :key="`category_${ category.id }`" :value="category.id">
                    @{{ category.name }}
                </option>
            </select>
        </div>
        <div class="filters__item">
            <label for="gender" class="filters__item-title">Gender</label>
            <select v-model="filters.gender_id" class="select" id="gender" :disabled="isLoading">
                <option v-for="gender in filterGenders" :key="`gender_${ gender.id }`" :value="gender.id">
                    @{{ gender.name }}
                </option>
            </select>
        </div>
        <div class="filters__item">
            <label for="birth_date" class="filters__item-title">Birth Date</label>
            <input v-model="filters.birth_date" id="birth_date" type="date" class="input input_date"
                   :disabled="isLoading">
        </div>
        <div class="filters__item">
            <label for="age" class="filters__item-title">Age</label>
            <input v-model="filters.age" id="age" type="number" min="0" class="input" :disabled="isLoading"
                   @change="setAge">
        </div>
        <div class="filters__item">
            <label for="age_range" class="filters__item-title">Age Range</label>
            <div id="age_range" class="filters__inputs-wrapper">
                <input v-model="filters.age_from" type="number" min="0" class="input" :disabled="isLoading"
                       @change="setAgeRange">
                <span class="filters__range-delimiter">-</span>
                <input v-model="filters.age_to" type="number" min="0" class="input" :disabled="isLoading"
                       @change="setAgeRange">
            </div>
        </div>
        <button type="submit"
                class="button button_success filters__button"
                :disabled="isLoading"
        >
            Apply
        </button>
        <button type="button"
                class="button button_warning filters__button"
                :disabled="isLoading"
                @click.prevent="exportData"
        >
            Export
        </button>
        <button type="button"
                class="button filters__button"
                :disabled="isLoading"
                @click.prevent="reset"
        >
            Reset
        </button>
    </form>
    <table class="table">
        <thead>
        <th>Category</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
        <th>Gender</th>
        <th>Birth Date</th>
        </thead>
        <tbody>
        <tr v-for="item in items" :key="`tr_${ item.id }`">
            <td>@{{ categoriesByKey[item.category.id] }}</td>
            <td>@{{ item.first_name }}</td>
            <td>@{{ item.last_name }}</td>
            <td>@{{ item.email }}</td>
            <td>@{{ gendersByKey[item.gender.id] }}</td>
            <td>@{{ item.birth_date }}</td>
        </tr>
        </tbody>
    </table>
    <div v-if="rawPagination.last_page > 1" class="app__pagination pagination">
        <button v-for="item in pagination"
                :key="`pagination_${ item }`"
                type="button"
                class="button pagination__item"
                :disabled="typeof item !== 'number'"
                @click.prevent="paginate(item)"
        >
            @{{ item }}
        </button>
    </div>
</div>
</body>
</html>
