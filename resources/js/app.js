import { createApp } from 'vue/dist/vue.esm-bundler';

createApp({
    data() {
        return {
            file: null,
            isLoading: false,
            items: [],
            filters: {
                category_id: null,
                gender_id: null,
                birth_date: null,
                age: null,
                age_from: null,
                age_to: null,
                page: 1,
            },
            categories: [],
            genders: [],
            rawPagination: {
                current_page: 1,
                last_page: 1,
            },
        };
    },

    async mounted() {
        this.isLoading = true;

        this.defaultFilters = { ...this.filters };

        // Load filters data
        await Promise.all([
            this.fetchCategories(),
            this.fetchGenders(),
        ]);

        // Load tables data
        await this.fetchItems();

        this.isLoading = false;
    },

    computed: {
        /**
         * Returns a list of categories for the filter.
         *
         * @returns {[{name: string, id: null}]}
         */
        filterCategories() {
            return [
                { id: null, name: 'All' },
                ...this.categories,
            ];
        },

        /**
         * Returns a list of genders for the filter.
         *
         * @returns {[{name: string, id: null}]}
         */
        filterGenders() {
            return [
                { id: null, name: 'All' },
                ...this.genders,
            ];
        },

        /**
         * Returns the URL to upload dataset file.
         *
         * @returns {*}
         */
        fileUploadUrl() {
            return this.$refs.uploadForm.action;
        },

        /**
         * Returns the URL to fetch available items.
         *
         * @returns {unknown}
         */
        fetchItemsUrl() {
            return this.$refs.uploadForm.getAttribute('data-fetch-items');
        },

        /**
         * Returns the URL to export available items.
         *
         * @returns {unknown}
         */
        exportItemsUrl() {
            return this.$refs.uploadForm.getAttribute('data-export-items');
        },

        /**
         * Returns the URL to fetch available categories.
         *
         * @returns {unknown}
         */
        fetchCategoriesUrl() {
            return this.$refs.uploadForm.getAttribute('data-fetch-categories');
        },

        /**
         * Returns the URL to fetch available categories.
         *
         * @returns {unknown}
         */
        fetchGendersUrl() {
            return this.$refs.uploadForm.getAttribute('data-fetch-genders');
        },

        /**
         * Returns categories by their names.
         */
        categoriesByKey() {
            const categories = {};

            this.categories.forEach((c) => {
                categories[c.id] = c.name;
            });

            return categories;
        },

        /**
         * Returns genders by their names.
         */
        gendersByKey() {
            const genders = {};

            this.genders.forEach((c) => {
                genders[c.id] = c.name;
            });

            return genders;
        },

        pagination() {
            const range = (min, max) => {
                return [
                    ...Array(max - min + 1)
                        .keys(),
                ].map(x => x + min);
            };

            let items = [];
            const totalElementsBySide = 4;
            const visibleElementsBySide = 2;
            const currentPage = this.rawPagination.current_page;
            const lastPage = this.rawPagination.last_page;

            if (currentPage - totalElementsBySide <= 0) {
                items = items.concat(...range(1, currentPage));
            } else {
                items = items.concat([
                    1,
                    '...',
                    ...range(currentPage - visibleElementsBySide, currentPage),
                ]);
            }

            if (lastPage - totalElementsBySide < currentPage) {
                items = items.concat(range(currentPage + 1, lastPage));
            } else {
                items = items.concat([
                    ...range(currentPage + 1, currentPage + visibleElementsBySide),
                    '...',
                    lastPage,
                ]);
            }

            return items;
        },

        /**
         * Makes the GET-parameter string with all parameters.
         *
         * @returns {string}
         */
        grubFilters() {
            const filters = {};
            Object.keys(this.filters).forEach((k) => {
                const value = this.filters[k];
                if (value !== null && `${ value }`.length > 0) {
                    filters[k] = value;
                }
            });

            const ageRange = [];
            if (this.filters.age !== null) {
                ageRange.push(this.filters.age);
                delete filters.age;
                this.filters.age_from = null;
                this.filters.age_to = null;
            } else {
                if (this.filters.age_from !== null || this.filters.age_to !== null) {
                    if (this.filters.age_from !== null) {
                        ageRange.push(this.filters.age_from);
                        delete filters.age_from;
                        this.filters.age = null;
                    }
                    if (this.filters.age_to !== null) {
                        ageRange.push(this.filters.age_to);
                        delete filters.age_to;
                        this.filters.age = null;
                    }
                }
            }

            const ageRangeParameter = ageRange.map(ar => `age_range[]=${ ar }`)
                .join('&');

            return `${ ageRangeParameter }&${ new URLSearchParams(filters) }`;
        },
    },

    methods: {
        /**
         * Changes the file to upload.
         *
         * @param e
         */
        fileChanged(e) {
            if (e.target && e.target.files) {
                this.file = e.target.files[0];
            }
        },

        /**
         * Uploads the client's dataset to the server.
         *
         * @returns {Promise<void>}
         */
        async upload() {
            this.isLoading = true;

            const formData = new FormData();
            formData.append('file', this.file);

            const response = await fetch(this.fileUploadUrl, {
                headers: {
                    'Accept': 'application/json',
                },
                method: 'POST',
                body: formData,
            });

            if (response.status === 422) {
                const body = await response.json();
                alert(body.message);
            } else if (response.status === 200) {
                await this.fetchItems();
            }

            this.isLoading = false;
        },

        /**
         * Fetches available categories.
         *
         * @returns {Promise<boolean>}
         */
        async fetchCategories() {
            this.isLoading = true;

            const response = await fetch(this.fetchCategoriesUrl);
            const { data } = await response.json();

            this.categories = data;

            this.isLoading = false;

            return true;
        },

        /**
         * Fetches available genders.
         *
         * @returns {Promise<boolean>}
         */
        async fetchGenders() {
            this.isLoading = true;

            const response = await fetch(this.fetchGendersUrl);
            const { data } = await response.json();

            this.genders = data;

            this.isLoading = false;

            return true;
        },

        /**
         * Fetches the items from the backend.
         *
         * @returns {Promise<boolean>}
         */
        async fetchItems() {
            this.isLoading = true;

            const response = await fetch(`${ this.fetchItemsUrl }?${ this.grubFilters }`);
            const data = await response.json();

            this.items = data.data;
            this.rawPagination = data.meta;

            this.isLoading = false;

            return true;
        },

        /**
         * Resets the filters.
         */
        reset() {
            this.filters = { ...this.defaultFilters };

            this.fetchItems();
        },

        /**
         * Setting null for age on the age range changes.
         */
        setAgeRange() {
            this.filters.age = null;
        },

        /**
         * Setting null for age rage on the age changes.
         */
        setAge() {
            this.filters.age_from = null;
            this.filters.age_to = null;
        },

        /**
         * Change the pagination and fetch the items.
         *
         * @param item
         */
        paginate(item) {
            this.filters.page = item;

            this.fetchItems();
        },

        /**
         * Export table and download the file.
         *
         * @returns {Promise<boolean>}
         */
        async exportData() {
            this.isLoading = true;

            const response = await fetch(`${ this.exportItemsUrl }?${ this.grubFilters }`);

            const data = await response.json();

            window.location.href = data.url;

            this.isLoading = false;

            return true;
        },
    },
}).mount('.app');

