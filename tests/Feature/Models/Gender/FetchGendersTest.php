<?php

namespace Tests\Feature\Models\Gender;

use Database\Seeders\Common\GenderSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class FetchGendersTest extends TestCase
{
    use RefreshDatabase;

    public function testFetchingGenders(): void
    {
        $this->seed(GenderSeeder::class);
        $response = $this->makeRequest();

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'name',
                ],
            ],
        ]);
    }

    /**
     * Makes a request to the URI.
     *
     * @param array $data
     * @param array $headers
     * @param int $options
     *
     * @return TestResponse
     */
    protected function makeRequest(array $data = [], array $headers = [], int $options = 0): TestResponse
    {
        $uri = route('genders.index', $data);

        return $this->getJson($uri, $headers, $options);
    }
}
