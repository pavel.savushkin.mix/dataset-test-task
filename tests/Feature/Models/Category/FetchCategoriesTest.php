<?php

namespace Tests\Feature\Models\Category;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class FetchCategoriesTest extends TestCase
{
    use RefreshDatabase;

    public function testFetchingCategories(): void
    {
        Category::factory(10)->create();
        $response = $this->makeRequest();

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'name',
                ],
            ],
        ]);
    }

    /**
     * Makes a request to the URI.
     *
     * @param array $data
     * @param array $headers
     * @param int $options
     *
     * @return TestResponse
     */
    protected function makeRequest(array $data = [], array $headers = [], int $options = 0): TestResponse
    {
        $uri = route('categories.index', $data);

        return $this->getJson($uri, $headers, $options);
    }
}
