<?php

namespace Tests\Feature\Models\User;

use App\Models\Category;
use App\Models\Gender;
use App\Models\User;
use Database\Seeders\DatabaseSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class FetchUsersTest extends TestCase
{
    use RefreshDatabase;

    public function testFetchingUsers(): void
    {
        $this->seed(DatabaseSeeder::class);
        $response = $this->makeRequest();

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'category' => [
                        'id',
                    ],
                    'gender' => [
                        'id',
                    ],
                    'first_name',
                    'last_name',
                    'email',
                    'birth_date',
                ],
            ],
            'meta' => [
                'current_page',
                'from',
                'last_page',
                'links',
                'path',
                'per_page',
                'to',
                'total',
            ],
        ]);
    }

    /**
     * Makes a request to the URI.
     *
     * @param array $data
     * @param array $headers
     * @param int $options
     *
     * @return TestResponse
     */
    protected function makeRequest(array $data = [], array $headers = [], int $options = 0): TestResponse
    {
        $uri = route('users.index', $data);

        return $this->getJson($uri, $headers, $options);
    }

    public function testFetchingUsersByCategory(): void
    {
        $this->seed(DatabaseSeeder::class);
        $categoryId = Category::query()->inRandomOrder()->first()->id;
        $response = $this->makeRequest(['category_id' => $categoryId]);

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'category' => [
                'id' => $categoryId,
            ],
        ]);
    }

    public function testFetchingUsersByGender(): void
    {
        $this->seed(DatabaseSeeder::class);
        $genderId = Gender::query()->inRandomOrder()->first()->id;
        $response = $this->makeRequest(['gender_id' => $genderId]);

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'gender' => [
                'id' => $genderId,
            ],
        ]);
    }

    public function testFetchingUsersByBirthDate(): void
    {
        $this->seed(DatabaseSeeder::class);
        $birthDate = User::query()
            ->select(['birth_date'])
            ->groupBy(['birth_date'])
            ->inRandomOrder()
            ->first()
            ->birth_date
            ->format('Y-m-d');
        $response = $this->makeRequest(['birth_date' => $birthDate]);

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'birth_date' => $birthDate,
        ]);
    }

    public function testFetchingUsersByAge(): void
    {
        $this->seed(DatabaseSeeder::class);
        $maxMinBirthDates = User::query()
            ->selectRaw('max(birth_date) as max, min(birth_date) as min')
            ->first();
        $currentYear = now()->year;
        $maxAge = $currentYear - Carbon::parse($maxMinBirthDates->min)->year;
        $minAge = $currentYear - Carbon::parse($maxMinBirthDates->max)->year;

        $requestedAge = rand($minAge, $maxAge);
        $requestedYear = now()->subYears($requestedAge)->year;

        $response = $this->makeRequest(['age_range' => [$requestedAge]]);

        $response->assertStatus(200);

        $data = collect($response->json()['data']);
        $hasWrongYears = $data->pluck('birth_date')
            ->filter(function ($date) use ($requestedYear) {
                $year = (int)substr($date, 0, 4);

                return $year !== $requestedYear;
            })
            ->isNotEmpty();
        $this->assertFalse($hasWrongYears);
    }

    public function testFetchingUsersByAgeRange(): void
    {
        $this->seed(DatabaseSeeder::class);
        $maxMinBirthDates = User::query()
            ->selectRaw('max(birth_date) as max, min(birth_date) as min')
            ->first();
        $currentYear = now()->year;
        $maxAge = $currentYear - Carbon::parse($maxMinBirthDates->min)->year;
        $minAge = $currentYear - Carbon::parse($maxMinBirthDates->max)->year;

        $requestedMinAge = rand($minAge, $maxAge);
        $requestedMinYear = now()->subYears($requestedMinAge)->year;
        do {
            $requestedMaxAge = rand($minAge, $maxAge);
        }
        while ($requestedMaxAge !== $requestedMinAge && $maxAge !== $minAge);
        $requestedMaxYear = now()->subYears($requestedMinAge)->year;

        // We do not care here if the `max age` < `min age`. It's covered by the implementation.
        $response = $this->makeRequest(['age_range' => [$requestedMinAge, $requestedMaxAge]]);

        $response->assertStatus(200);

        $data = collect($response->json()['data']);
        $hasWrongYears = $data->pluck('birth_date')
            ->filter(function ($date) use ($requestedMinYear, $requestedMaxYear) {
                $year = (int)substr($date, 0, 4);

                return $year < $requestedMinYear || $year > $requestedMaxYear;
            })
            ->isNotEmpty();
        $this->assertFalse($hasWrongYears);
        $this->assertFalse($data->isEmpty());
    }
}
