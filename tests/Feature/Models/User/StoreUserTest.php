<?php

namespace Tests\Feature\Models\User;

use App\Models\Category;
use App\Models\Gender;
use App\Models\User;
use Database\Seeders\Common\GenderSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class StoreUserTest extends TestCase
{
    use RefreshDatabase;

    public function testLostUploadingFile(): void
    {
        $response = $this->makeRequest();

        $response->assertStatus(422);
        $response->assertJsonStructure(['errors' => ['file']]);
    }

    /**
     * Makes a request to the URI.
     *
     * @param array $data
     * @param array $headers
     * @param int $options
     *
     * @return TestResponse
     */
    protected function makeRequest(array $data = [], array $headers = [], int $options = 0): TestResponse
    {
        $uri = route('users.store');

        return $this->postJson($uri, $data, $headers, $options);
    }

    public function testNullableUploadingFile(): void
    {
        $response = $this->makeRequest(['file' => null]);

        $response->assertStatus(422);
        $response->assertJsonStructure(['errors' => ['file']]);
    }

    public function testIncorrectTypeUploadingFile(): void
    {
        $response = $this->makeRequest(['file' => UploadedFile::fake()->create('dataset.xls')]);

        $response->assertStatus(422);
        $response->assertJsonStructure(['errors' => ['file']]);

        $response = $this->makeRequest(['file' => UploadedFile::fake()->create('dataset.xlsx')]);

        $response->assertStatus(422);
        $response->assertJsonStructure(['errors' => ['file']]);
    }

    public function testCorrectUploadingFile(): void
    {
        $response = $this->makeRequest(['file' => UploadedFile::fake()->create('dataset.txt')]);

        $response->assertStatus(204);

        $response = $this->makeRequest(['file' => UploadedFile::fake()->create('dataset.csv')]);

        $response->assertStatus(204);
    }

    public function testUploadingFileWithNewDataThatShouldBeCreated(): void
    {
        $this->seed(GenderSeeder::class);

        $category = Str::uuid();
        $content = 'category,firstname,lastname,email,gender,birthDate' . PHP_EOL;
        $content .= $category . ',test,test,test@example.com,male,1980-01-01';

        $response = $this->makeRequest(['file' => UploadedFile::fake()->createWithContent('dataset.txt', $content)]);
        $response->assertStatus(204);

        $this->assertDatabaseHas(Category::class, ['name' => $category]);

        $category = Category::query()->where('name', $category)->first();
        $gender = Gender::query()->where('name', 'male')->first();
        $this->assertDatabaseHas(User::class, [
            'category_id' => $category->id,
            'gender_id' => $gender->id,
            'first_name' => 'test',
            'last_name' => 'test',
            'email' => 'test@example.com',
            'birth_date' => '1980-01-01',
        ]);
    }

    public function testUploadingFileWithData(): void
    {
        $this->seed(GenderSeeder::class);
        $categories = Category::factory(50)->create();
        $genders = Gender::query()->get();

        $datasetSize = 1000;

        $content = 'category,firstname,lastname,email,gender,birthDate';
        $itemToTest = [];
        while ($datasetSize > 0) {
            $datasetSize--;

            $category = $categories->random();
            $gender = $genders->random();

            if (is_null($itemToTest)) {
                $itemToTest = [
                    'category_id' => $category->id,
                    'first_name' => $itemToTest[1],
                    'last_name' => $itemToTest[2],
                    'email' => $itemToTest[3],
                    'gender_id' => $gender->id,
                    'birth_date' => $itemToTest[5],
                ];
            }

            $content .= implode(',', [
                    $category->name,
                    fake()->firstName,
                    fake()->lastName,
                    fake()->email,
                    $gender->name,
                    fake()->date('Y-m-d', '-10 years'),
                ]) . PHP_EOL;
        }

        $response = $this->makeRequest(['file' => UploadedFile::fake()->createWithContent('dataset.txt', $content)]);

        $response->assertStatus(204);
        $this->assertDatabaseHas(User::class, $itemToTest);
    }

    protected function setUp(): void
    {
        parent::setUp();

        Storage::fake('local');
    }
}
