<?php

return [
    'chunk_size' => (int) env('PROCESSING_DATASET_CHUNK_SIZE', 10000),
];
