<?php

namespace Database\Seeders\Common;

use App\Models\Gender;
use Illuminate\Database\Seeder;

class GenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Gender::query()->insert([
            ['name' => 'male'],
            ['name' => 'female'],
            ['name' => 'none'],
        ]);
    }
}
