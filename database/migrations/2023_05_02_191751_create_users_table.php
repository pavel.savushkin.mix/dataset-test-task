<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->foreignId('category_id')->comment('Client\'s favorite category');
            $table->foreignId('gender_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->date('birth_date');

            $table->foreign('gender_Id')
                ->references('id')
                ->on('genders');
            $table->foreign('category_id')
                ->references('id')
                ->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
