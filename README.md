# Dataset - Test Task

## Requirements
1. Docker ([installation doc](https://docs.docker.com/engine/install/))
2. Docker Compose ([installation doc](https://docs.docker.com/compose/install/other/))

## Usage

To run the project you need (all command should be run from the root of the local repository):
1. Set up the `.env` file in the `provision` directory (the directory contains all Docker configurations):
```shell
cp provision/.env.example provision/.env
```
2. Build and run the docker images:
```shell
cd provision; \
docker-compose up -d; \
cd ..;
```
3. Copy and **edit** the `.env` configuration of the application:
```shell
cp .env.example .env
```
4. Install dependencies:
```shell
cd provision; \
docker-compose exec php-fpm composer install; \
cd ..;
```
5. Run migrations:
```shell
cd provision; \
docker-compose exec php-fpm php artisan migrate; \
cd ..;
```
6. Make link for the public storage:
```shell
cd provision; \
docker-compose exec php-fpm php artisan storage:link; \
cd ..;
```

Note: make sure `APP_URL` has the correct value (the same port as `provision/.env` -> `NGINX_PORT` is specified).

## Testing
To run tests, use the next command:
```shell
cd provision; \
docker-compose exec php-fpm php artisan test; \
cd ..;
```
