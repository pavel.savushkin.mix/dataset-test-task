<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\GenderController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::apiResource('categories', CategoryController::class)
    ->only(['index']);
Route::apiResource('genders', GenderController::class)
    ->only(['index']);
Route::apiResource('users', UserController::class)
    ->only(['index', 'store']);
Route::get('users/export', [
    'as' => 'users.export',
    'uses' => UserController::class . '@export'
]);
