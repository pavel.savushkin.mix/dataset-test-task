<?php

use App\Http\Controllers\IndexController;
use Illuminate\Support\Facades\Route;

Route::get('', [
    'as' => 'index',
    'uses' => IndexController::class . '@index',
]);
